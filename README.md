# underfunded-rocketry

Mini Jam 122: Intermission game jam submission, can be found at https://t4rmin.itch.io/underfunded-rocketry.

To build this game, download the Godot Engine (https://godotengine.org) and refer to https://docs.godotengine.org/en/stable/development/compiling/index.html for compiling (current supported version is 3.5.1).