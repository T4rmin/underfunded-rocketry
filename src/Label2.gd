extends Label


var leave_earth: bool = false;
var dialogues_fail: Array = [
	"More money? These funds already put you in space!\nThe best we can do is a couple more.",
	"One of these days you will succed, I guess.",
	"Our vaults of pure gold have decreased by 0.01% in value.\nHope you're happy.",
	"Funds? In stacks of gold or diamonds?",
	"With these funds you better bring me a new element by tomorrow."
];
var dialogues_earth: Array = [
	"Maybe it's your astronout that sucks at doing their job.\nThe fuel is not enough? Nah.",
	"Even more? C'mon, do our villas and jacuzzis not important to you?",
	"Alright, this is getting ridiculous now.",
	"How.",
	"A firing is in order, but it seems there's no one to replace you.\nSo... keep doing your worst.",
	"Where did it land, anyway?",
	"Better hope its not on someone's backyard.",
	"...",
];
var dialogues_success: Array = [
	"Nice!\nSo no budget increase?",
	"Finally.",
	"I hope this means more government subsidies for us to embezzle- I mean to fund our rockets.",
];
var rng = RandomNumberGenerator.new()
var add: float;

signal addition(cashmoney);

# Called when the node enters the scene tree for the first time.
func _ready():
	rng.randomize();
	var where = Global.spaceship_finish;
	if where == "Gaia":
		self.text = dialogues_earth[Global.earth_counter];
		if Global.earth_counter == 7:
			Global.earth_counter -= 1;
		Global.earth_counter += 1;
		Global.tries += 1;
		Global.money += 20;
		add = 20;
	elif where == Global.target:
		Global.change();
		if Global.mission >= 6:
			pass
		add = 0;
		self.text = dialogues_success[rng.randi_range(0,2)];
	else:
		self.text = dialogues_fail[rng.randi_range(0,4)];
		Global.tries += 1;
		add = rng.randi_range(3, 8) * 25
		Global.money += add;


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
