extends Node2D
class_name Ship


export var acceleration: float;
export var fuel_tank: float;
export var rotation_speed: float;
export var burn_rate: float;
export var initial_speed: Vector2;

var speed: Vector2 = Vector2.ZERO;
var burn: float = 0.0;
var lose: bool = false;
var external_influence: Vector2;

onready var fuel: float = Global.fuel_tank;
onready var burn_sprite = get_node("Burn");
onready var body_selected: Sprite = get_parent().get_node("Bodies/Sun/Sprite");

signal intermission;
signal direction(direct);

func _ready():
	speed += initial_speed;


func _input(event):
	pass;


func _process(delta):
	if Input.is_action_pressed('accelerate'):
		burn += 0.1 * delta;
	if Input.is_action_pressed('decelerate'):
		burn -= 0.1 * delta;
	if Input.is_action_pressed('left'):
		rotation -= rotation_speed * delta;
	if Input.is_action_pressed('right'):
		rotation += rotation_speed * delta;
	if Input.is_action_pressed('cut'):
		burn = 0;
	burn = clamp(burn, 0.0, 1.0);
	fuel -= burn * Global.burn_rate * delta;
	fuel = clamp(fuel, 0.0, Global.fuel_tank);
	if fuel == 0:
		burn = 0;
	if burn == 0:
		burn_sprite.visible = false;
	else:
		burn_sprite.visible = true;
	speed += burn * Global.acceleration * Vector2(cos(rotation), sin(rotation)) * delta;
	external_influence = get_parent().gravitational_acceleration();
	speed += external_influence;
	emit_signal("direction", speed);
	if lose == true:
		self.scale -= Vector2(2, 2) * delta;
		if scale <= Vector2.ZERO:
			emit_signal("intermission");
	global_position += speed * delta;


func _on_Solar_System_spaceship_singal(external_accel):
	external_influence = external_accel;


func _on_Body_select(body):
	body_selected = body.get_node("Sprite");


func _on_Body_area_enter(body_2):
	Global.spaceship_finish = body_2;
	print(Global.spaceship_finish)
	lose = true;
