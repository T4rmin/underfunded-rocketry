extends Label


onready var planet = get_parent().get_parent().get_node("Bodies/Aphrodite/Sprite");
onready var camera = get_parent().get_parent().get_node("Camera2D");
onready var spaceship = get_parent().get_parent().get_node("Spaceship");


func _process(delta):
	set_position(Vector2(planet.global_position.x + 100, planet.global_position.y));
	self.rect_scale = camera.zoom;
	self.text = "APHRODITE" + "\n" + str(round(spaceship.global_position.distance_to(planet.global_position))) + " U";
