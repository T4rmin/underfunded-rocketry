extends CanvasLayer


onready var spaceship: Ship = get_parent().get_node("Spaceship");
onready var counter = get_node("Fuel")
onready var arrow = get_node("Arrow")
onready var arrow_boogaloo = get_node("Arrow2")
onready var arrow_triquel = get_node("Arrow3")
onready var label_speed = get_node("LabelSpeed")
onready var burn = get_node("Burn")
onready var abandon = get_node("AbandonText")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _process(delta):
	counter.text = str(round(spaceship.fuel));
	label_speed.text = str(round(spaceship.speed.length())) + " U/s";
	arrow_boogaloo.rotation = spaceship.rotation;
	arrow_triquel.rotation = spaceship.external_influence.angle();
	burn.text = str(round(spaceship.burn * 100)) + "%";
	if spaceship.fuel == 0:
		abandon.visible = true;


func _on_Spaceship_direction(direct):
	arrow.rotation = direct.angle();
