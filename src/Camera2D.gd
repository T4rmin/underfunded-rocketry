extends Camera2D


onready var player: Ship = get_parent().get_node("Spaceship");

export var zoom_factor: Vector2 = Vector2(0.25, 0.25);


# Called when the node enters the scene tree for the first time.
func _ready():
	zoom = zoom_factor;


func _input(event):
	pass;


func _process(delta):
	position = player.position;


func _on_VSlider_value_changed(value):
	zoom = value * zoom_factor;
