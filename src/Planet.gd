extends Node2D

class_name Planet

export var mass: float;
export var speed: float;
export var rotator: float;

var radius: float;

onready var planet = get_node("Sprite");
onready var select = planet.get_node("Select");

signal select(body);
signal area_enter(body_2);

func _ready():
	radius = planet.global_position.length();


func gravitational_calculation(coordinates: Vector2, grav_const: float) -> Vector2:
	var grav_accel = (grav_const * mass) / pow(coordinates.distance_to(planet.global_position), 2);
	return grav_accel * (planet.global_position - coordinates).normalized();
	
	
func _process(delta):
	planet.rotation += rotator * delta;
	rotation += speed * delta;
	select.rotation += 1.0 * delta;
	select.modulate.a = (sin(OS.get_ticks_msec() * delta / 4) / 4) + 0.4;


func _on_Area2D_input_event(viewport, event, shape_idx):
	if event.is_action_pressed("select"):
		emit_signal("select", self);


func _on_Area2D_area_entered(area):
	emit_signal("area_enter", self.name);
