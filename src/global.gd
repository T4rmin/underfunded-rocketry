extends Node


var spaceship_finish: String = "space";
var target: String = "Aphrodite";
var mission: int = 1;
var tries: int = 1;
var earth_counter: int = 0;
var money: int = 0;

# Spaceship stats.
var acceleration: float = 25;
var fuel_tank: float = 100;
var burn_rate: float = 50;

var elapsed: float;



# Called when the node enters the scene tree for the first time.
func change():
	tries = 1;
	mission += 1;
	match mission:
		1:
			target = "Aphrodite";
		2:
			target = "Zeus";
		3:
			target = "Ares";
		4:
			target = "Kronos";
		5:
			target = "Hermes";
		6:
			get_tree().change_scene_to(load('res://Ending.tscn'))


func _process(delta):
	elapsed += delta;
