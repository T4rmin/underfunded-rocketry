extends Line2D


export var length: float = 1000;

var point: Vector2 = Vector2();

onready var spaceship: Ship = get_parent().get_node("Spaceship");


func _ready():
	global_position = Vector2(0, 0);
	global_rotation = 0;
	self.clear_points();


func _process(delta):
	point = spaceship.position;
	add_point(point);
	while get_point_count() > length:
		remove_point(0);
