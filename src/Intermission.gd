extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _input(event):
	if event.is_action_pressed('ui_accept'):
		get_tree().change_scene_to(load('res://Solar_System.tscn'))


func _on_Accel_input_event(viewport, event, shape_idx):
	if event.is_action_pressed('select'):
		Global.acceleration += 10;
		if Global.money >= 100:
			Global.money -= 100;


func _on_Fuel_input_event(viewport, event, shape_idx):
	if event.is_action_pressed('select'):
		Global.fuel_tank += 20;
		if Global.money >= 20:
			Global.money -= 20;


func _on_Effi_input_event(viewport, event, shape_idx):
	if event.is_action_pressed('select'):
		Global.burn_rate /= 2;
		if Global.money >= 1000:
			Global.money -= 1000;
