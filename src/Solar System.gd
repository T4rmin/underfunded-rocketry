extends Node2D


onready var spaceship: Ship = get_node("Spaceship");
onready var bodies = get_node("Bodies");
onready var map = get_node("CanvasLayer/Map");

var gravitational_acceleration: Vector2 = Vector2.ZERO;
var selected_body: Planet;

const GRAV_CONST = 0.01;


func _ready():
	pass


func _input(event):
	if event.is_action_pressed('abandon'):
		get_tree().change_scene_to(load('res://Intermission.tscn'))


func gravitational_acceleration() -> Vector2:
	gravitational_acceleration = Vector2.ZERO;
	var bodies = get_node("Bodies");
	for x in bodies.get_children():
		gravitational_acceleration += x.gravitational_calculation(spaceship.position, GRAV_CONST);
	return gravitational_acceleration;


func _on_Spaceship_intermission():
	get_tree().change_scene_to(load('res://Intermission.tscn'))


func _on_Body_select(body):
	if selected_body != null:
		selected_body.get_node("Sprite").get_node("Select").visible = false;
	selected_body = body;
	selected_body.get_node("Sprite").get_node("Select").visible = true;


func _on_Body_area_enter(body_2):
	pass # Replace with function body.
